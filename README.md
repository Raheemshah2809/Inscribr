<h1 align="center">Inscribr</h1>

<p align="center">
A fast and flexible web-based todo list app.
<br>
<a href="https://inscribr.netlify.app/"><strong>Homepage</strong></a>
</p>

<img style="border-radius:10px;" src=""><img width="1626" alt="Mainhome" src="https://user-images.githubusercontent.com/66431658/217643110-7306eef2-e05b-41b3-864d-ab96b409d125.png">


## Features

### 🏷️ Todo Properties

Create todo with 4 properties: Name, Date, Priority and Tag. Press Enter to add it to list. Click the circle to mark it as done.

### 🗂️ Multiple Lists

Create multiple lists for different purposes: daily todo list, long-term goal list or even project feature list.

### 🗓️ Calendar View

View your todos with calendar. The date with todos is highlighted. Click on each date to only show the todos on that day.

### 🗃️ Sorter

Sort your todos with the 4 properties. You can also use it under calendar view.

